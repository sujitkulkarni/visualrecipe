import {
  Injectable
} from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  DatashareService
} from './datashare.service';

@Injectable()
export class AuthGuard implements CanActivate {
  authSubscr: Subscription;
  auth: Object;

  constructor(private dataShareService: DatashareService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const url: string = state.url;
    this.auth = localStorage.getItem('VRAUTH');
    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {    
    console.log('authObj', this.auth);
    if (this.auth) {
      return true;
    }

    // Store the attempted URL for redirecting
    this.dataShareService.redirectUrl = url;

    // Navigate to the login page with extras
    this.router.navigate(['/login']);
    return false;
  }
}
