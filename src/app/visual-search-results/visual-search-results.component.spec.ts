import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualSearchResultsComponent } from './visual-search-results.component';

describe('VisualSearchResultsComponent', () => {
  let component: VisualSearchResultsComponent;
  let fixture: ComponentFixture<VisualSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
