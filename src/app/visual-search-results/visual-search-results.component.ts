import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { DatashareService } from '../datashare.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-visual-search-results',
  templateUrl: './visual-search-results.component.html',
  styleUrls: ['./visual-search-results.component.css']
})
export class VisualSearchResultsComponent implements OnInit {
  @Input() things: any;
  results: any;
  selectedThing: Object;
  ingredients: Array < Object > = [];
  units: Array < Object > = [];
  defaultValues: Object;
  ownIngredientName: String;
  private lastSelectedThing: Object;

  constructor(private datashare: DatashareService) {
    this.lastSelectedThing = {'id' : '-1'};
    this.ownIngredientName = '';
    this.units = [{
      'id': '1',
      'name': 'teaspoon'
    }, {
      'id': '2',
      'name': 'tablespoon'
    }, {
      'id': '3',
      'name': 'ounce'
    }, {
      'id': '4',
      'name': 'cup'
    }, {
      'id': '5',
      'name': 'litres'
    },{
      'id': '6',
      'name': 'kilograms'
    }, {
      'id': '7',
      'name': 'units'
    }];

    this.defaultValues = {
      'unit' : this.units[6],
      'selected' : false,
      'size' : 0
    };
  }

  ngOnInit() {
    console.log('visual component', this.things);
    this.selectedThing = {...this.defaultValues};
  }

  selectThing(thing: Object) {
    this.ownIngredientName = '';
    if (this.lastSelectedThing['id'] !== thing['id']) {
      this.lastSelectedThing['selected'] = false;
      thing['selected'] = true;
      thing['unit'] = this.defaultValues['unit'];
      thing['size'] = this.defaultValues['size'];
      this.selectedThing = JSON.parse(JSON.stringify(thing));
      this.lastSelectedThing = thing;
    }
  }

  selectUnit(unit: Object){
    this.selectedThing['unit'] = unit;
  }

  addIngredient(ownIngredientName: String, selectedThing: Object){
    if(ownIngredientName){
      selectedThing['name'] = ownIngredientName;
      selectedThing['id'] = this.ingredients.length + 1;
    }
    this.ingredients.push(selectedThing);
    this.datashare.updateData(this.ingredients);
    this.things.length = 0;
    this.ownIngredientName = '';
    this.selectedThing = {...this.defaultValues};
  }

  deleteIngredient(ingredient: Object){
    const index = this.ingredients.indexOf(ingredient);
    this.ingredients.splice(index, 1);
    this.datashare.updateData(this.ingredients);
  }

}
