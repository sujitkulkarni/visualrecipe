import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { DatashareService } from '../datashare.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ApiService]
})
export class LoginComponent implements OnInit {

  userEmail: string;
  userPassword: String;
  constructor(private apiService: ApiService, private dataShareService: DatashareService, private router: Router) {
    this.userEmail = '';
    this.userPassword = '';
  }

  ngOnInit() {
  }

  login(){
    this.apiService.login(this.userEmail, this.userPassword)
      .then(data => {
        console.log('logged in', data);
        this.dataShareService.updateAuth(data);
        const url = this.dataShareService.redirectUrl ? this.dataShareService.redirectUrl : '/';
        this.router.navigate([url]);

      })
      .catch(error => {
        console.log('error', error);
      });
  }

}
