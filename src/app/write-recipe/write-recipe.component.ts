import {
  Component,
  OnInit,
  OnDestroy,
  Input
} from '@angular/core';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  DatashareService
} from '../datashare.service';
import {
  ApiService
} from '../api.service';

@Component({
  selector: 'app-write-recipe',
  templateUrl: './write-recipe.component.html',
  styleUrls: ['./write-recipe.component.css']
})
export class WriteRecipeComponent implements OnInit, OnDestroy {
  @Input() username: any;
  title = 'My Awesome Recipe';
  ingredients = [];
  prepSteps = [];
  ingrSubscription: Subscription;
  prepSubscription: Subscription;
  constructor(private dataShare: DatashareService, private apiService: ApiService) {
    this.ingrSubscription = this.dataShare._ingredients.subscribe(
      ingr => {
        this.ingredients = ingr;
      }
    );

    this.prepSubscription = this.dataShare._prepSteps.subscribe(
      steps => {
        this.prepSteps = steps;
      }
    );
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.dataShare.updateRecipe({
      'title': this.title,
      'ingredients': this.ingredients,
      'prepsteps': this.prepSteps,
      'footNote': this.dataShare.footNote,
      'cookingTime': this.dataShare.cookingTime
    });
  }

  publish() {
    let postObject: any = {
      'title': this.title,
      'ingredients': this.ingredients,
      'prepsteps': this.prepSteps,
      'footNote': this.dataShare.footNote,
      'cookingTime': this.dataShare.cookingTime
    };

    this.apiService.saveRecipe(postObject)
      .then(res => {
        console.log('saved', res);
        postObject = {};
      })
      .catch(error => {
        console.log('error', error);
      });
  }

}
