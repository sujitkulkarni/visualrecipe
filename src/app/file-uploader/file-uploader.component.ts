import {
  Component,
  ViewChild,
  ElementRef,
  OnInit,
  AfterViewInit
} from '@angular/core';

import {
  ApiService
} from '../api.service';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.css'],
  providers: [ApiService]
})
export class FileUploaderComponent implements AfterViewInit {
  @ViewChild('srcImage') srcImage: ElementRef;
  @ViewChild('imgPreview') imgPreview: ElementRef;
  @ViewChild('previewCanvas') previewCanvas: ElementRef;
  public context: CanvasRenderingContext2D;

  things: any = [];
  imgSrc: String = '';
  showAlert: Boolean = false;
  message: String = 'Guessing what it is!';
  fetchedThings: Boolean = true;
  canvasOptions: Object = {};

  constructor(private apiService: ApiService) {}

  ngOnInit(){
    this.canvasOptions = {
      'width' : 200,
      'height' : 200
    };
  }
  
  ngAfterViewInit() {
    this.context = this.previewCanvas.nativeElement.getContext('2d');
  }

  useImageBtn = () => {
    console.log('clicked', this.srcImage.nativeElement);
    const file = this.srcImage.nativeElement.files[0],
      r = new FileReader();

    if(!file){
      this.message = 'Please select a valid image';
      this.showAlert = true;
    } else {
      r.onloadend = (e: any) => {
        const blob = e.target.result;
        this.drawOnCanvas(blob);
        this.message = '';
      }
      r.readAsDataURL(file);
    }

  }

  assignThingsValue() {
    for (let i = 0; i < this.things.length; i++) {
      if (this.things[i].value) {
        this.things[i].widthValue = (this.things[i].value * 100).toFixed(2) + '%';
      }
    }
  }

  drawOnCanvas = (blob) => {
    console.log('input length', blob.length);
    let img = new Image();
    this.context.clearRect(0, 0, this.canvasOptions['width'], this.canvasOptions['height']);
    img.onload = () => {
      this.context.drawImage(img, 0, 0, this.canvasOptions['width'], this.canvasOptions['height']);
      this.doVisualSearch(this.previewCanvas.nativeElement.toDataURL());
    }
    img.src = blob;
  }

  doVisualSearch = (canvasBlob) => {
    console.log('searching what is this');
      this.apiService.doVisualSearch(canvasBlob)
        .then(things => {
          console.log('outputs', things.outputs[0]);
          this.things = things.outputs[0].data.concepts;
          this.message = 'We tried to guess what it is. Go to next step';
          this.assignThingsValue();
          this.fetchedThings = true;
        })
        .catch(error => {
          console.log('error', error);
          this.message = 'Something went wrong!';
          this.fetchedThings = false;
        });
      this.showAlert = true;
  }
}

