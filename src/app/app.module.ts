import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';
import { VisualSearchResultsComponent } from './visual-search-results/visual-search-results.component';
import { RecipeComponent } from './recipe/recipe.component';
import { PreparationStepsComponent } from './preparation-steps/preparation-steps.component';
import { HttpModule } from '@angular/http';
import { ApiService } from './api.service';
import { DatashareService } from './datashare.service';
import { WriteRecipeComponent } from './write-recipe/write-recipe.component';
import { PreviewRecipeComponent } from './preview-recipe/preview-recipe.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    FileUploaderComponent,
    VisualSearchResultsComponent,
    RecipeComponent,
    PreparationStepsComponent,
    WriteRecipeComponent,
    PreviewRecipeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    AppRoutingModule
  ],
  providers: [ApiService, DatashareService],
  bootstrap: [AppComponent]
})
export class AppModule { }
