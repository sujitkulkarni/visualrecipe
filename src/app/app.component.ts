import {
  Component,
  OnInit
} from '@angular/core';

import {
  Subscription
} from 'rxjs/Subscription';
import {
  DatashareService
} from './datashare.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  authSubscr: Subscription;
  username: Object;
  title = 'My Awesome Recipe';

  constructor(private dataShareService: DatashareService) {
    this.authSubscr = this.dataShareService._auth.subscribe(
        authObj => {
          this.username = authObj.username;
        }
      );
  }
  ngOnInit() {}


}
