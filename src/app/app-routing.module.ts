import {
  NgModule
} from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';
import {
  AuthGuard
} from './auth-guard.service';

import {
  WriteRecipeComponent
} from './write-recipe/write-recipe.component';
import {
  PreviewRecipeComponent
} from './preview-recipe/preview-recipe.component';
import {
  LoginComponent
} from './login/login.component';


const appRoutes: Routes = [{
    path: 'preview-recipe',
    canActivate: [AuthGuard],
    component: PreviewRecipeComponent
  },
  {
    path: 'write-recipe',
    canActivate: [AuthGuard],
    component: WriteRecipeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: '/write-recipe',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: WriteRecipeComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes, {
        enableTracing: true
      } // <-- debugging purposes only
    )
  ],
  providers: [AuthGuard],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
