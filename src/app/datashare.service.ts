import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DatashareService {
  private ingredients = new Subject<any>();
  private prepSteps = new Subject<any>();
  private auth = new Subject<any>();
  public recipe: Object;
  public cookingTime: Object;
  public footNote: String;
  public redirectUrl: String;
  _ingredients = this.ingredients.asObservable();
  _prepSteps = this.prepSteps.asObservable();
  _auth = this.auth.asObservable();

  updateData(ingredients:any) {
    console.log('service ingredients');
    this.ingredients.next(ingredients);
    console.log('ingredients', this._ingredients);
  }

  addStep(step:any) {
    console.log('service step');
    this.prepSteps.next(step);
    console.log('steps', this._prepSteps);
  }

  addFootNoteAndTiming(cookingTime: Object, footNote: String){
    this.cookingTime = cookingTime;
    this.footNote = footNote;
  }

  updateRecipe(recipeObj: Object){
    console.log('service recipe');
    this.recipe = recipeObj;
    console.log('recipe', this.recipe);
  }

  updateAuth(auth: Object){
    console.log('service auth');
    this.auth.next(auth);
    localStorage.setItem('VRAUTH', JSON.stringify(auth));
    console.log('auth', this._auth);
  }
  
}
