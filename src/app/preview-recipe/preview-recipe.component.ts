import {
  Component,
  OnInit
} from '@angular/core';
import {
  DatashareService
} from '../datashare.service';
import {
  ApiService
} from '../api.service';


@Component({
  selector: 'app-preview-recipe',
  templateUrl: './preview-recipe.component.html',
  styleUrls: ['./preview-recipe.component.css']
})
export class PreviewRecipeComponent implements OnInit {
  title = '';
  ingredients = [];
  prepSteps = [];
  cookingTime: Object;
  footNote: String;
  constructor(private dataShare: DatashareService, private apiService: ApiService) {}

  ngOnInit() {
    this.title = this.dataShare.recipe['title'];
    this.ingredients = this.dataShare.recipe['ingredients'];
    this.prepSteps = this.dataShare.recipe['prepsteps'];
    this.cookingTime = this.dataShare.recipe['cookingTime'];
    this.footNote = this.dataShare.recipe['footNote'];
  }

  publish() {
    this.apiService.saveRecipe(this.dataShare.recipe)
      .then(res => {
        console.log('saved', res);
      })
      .catch(error => {
        console.log('error', error);
      });
  }

}
