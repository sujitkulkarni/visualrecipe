import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationStepsComponent } from './preparation-steps.component';

describe('PreparationStepsComponent', () => {
  let component: PreparationStepsComponent;
  let fixture: ComponentFixture<PreparationStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparationStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparationStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
