import {
  Component,
  OnInit
} from '@angular/core';
import {
  DatashareService
} from '../datashare.service';

@Component({
  selector: 'app-preparation-steps',
  templateUrl: './preparation-steps.component.html',
  styleUrls: ['./preparation-steps.component.css']
})
export class PreparationStepsComponent implements OnInit {
  prepSteps: Array<Object>;
  stepObj: Object;
  newStep: String;
  cookingTime: Object;
  footNote: String;
  constructor(private dataShare: DatashareService) {
    this.prepSteps = [];
    this.stepObj = {};
    this.newStep = '';
    this.cookingTime = {
      'hours' : 0,
      'minutes' : 0
    };
    this.footNote = '';
  }

  ngOnInit() {}

  addStep(newStep: String) {
    this.prepSteps.push({
      'id': (this.prepSteps.length + 1).toString(),
      'text': newStep
    });
    this.newStep = '';
    this.dataShare.addStep(this.prepSteps);
  }

  removeStep(step: Object){
    const index = this.prepSteps.indexOf(step);
    this.prepSteps.splice(index, 1);
  }

  preventEvent(event: any) {
    event.preventDefault();
  }

  addFootNoteAndTiming(cookingTime: Object, footNote: String){
    this.dataShare.addFootNoteAndTiming(cookingTime, footNote);
  }

}
