import {
  Injectable
} from '@angular/core';
import {
  Http,
  Response,
  Headers,
  RequestOptions
} from '@angular/http';
import 'rxjs/add/operator/toPromise';

const host = 'http://localhost:3000/api/';
@Injectable()
export class ApiService {
  constructor(
    private http: Http
  ) {}

  doVisualSearch(base64): Promise < any > {
    // return this.http.post('/api/whatisthis', {'base64' : base64})
    // .map(res => res.json());
    return this.http.post(host + 'whatisthis', {'base64' : base64})
      .toPromise()
      .then(this.onSuccess)
      .catch(this.handleError);
  }

  saveRecipe(recipe): Promise < any > {
    return this.http.post(host + 'saverecipe', {'recipe' : recipe})
      .toPromise()
      .then(this.onSuccess)
      .catch(this.handleError);
  }

  login(email, pwd): Promise < any > {
    return this.http.post(host + 'login', {'username' : email, 'password' : pwd})
      .toPromise()
      .then(this.onSuccess)
      .catch(this.handleError);
  }

  private onSuccess(res: Response) {
    const body = res.json();
    return body || {};
  }

  private handleError(error: any): Promise < any > {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
