# Visualrecipe

Click the pictures of the ingredients, and our app engine will **guess** what it is, Then you add it- to the list of ingredients, jot down **prep steps**, and publish your recipe.


# Installation
Clone the repo first, and then

    npm install
Have angular-cli installed and then, from command line, go to the app folder and hit

    ng serve -o
Run the app at http://localhost:4200
