const express = require('express');
const router = express.Router();
const tinify = require('tinify');
var mongoose = require('mongoose');
const MongoClient = require('mongodb').MongoClient;
var fs = require('fs');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var session = require('express-session');

const Clarifai = require('clarifai');
var clarifai_err_log = fs.createWriteStream(__dirname + '/clarifai_err_log.log', {
  flags: 'a'
});
var tinify_err_log = fs.createWriteStream(__dirname + '/tinify_err_log.log', {
  flags: 'a'
});

tinify.key = 'JwBT_FVb4MpS3gpFk07y0TOq9QB0tS9m';

var db;
MongoClient.connect('mongodb://admin:admin123@ds149998.mlab.com:49998/sujitcrudprojects', function (err, client) {
	if (err) {
		return console.log(err);
	}
  db = client.db('sujitcrudprojects');
  console.log('db connected');
});
const clarifaiApp = new Clarifai.App({
  apiKey: 'ca235a8a8acc4cb8b447d15e68af0650'
});

// passport needs ability to serialize and unserialize users out of session
passport.serializeUser(function (user, done) {
  done(null, user.id);
});
passport.deserializeUser(function (id, done) {
  done(null, id);
});

// passport local strategy for local-login, local refers to this app
passport.use('local-login', new LocalStrategy(function (username, password, done) {
    // check in mongo if a user with username exists or not
    console.log('check if '+username+' exists');
    db.collection('userinfo').findOne({
      'username': username
    }, function (err, user) {
      console.log('usermodel err',err);
      console.log('usermodel usr',user);
      if (err) {
        return done(err);
      }

      if (!user) {
        console.log(username + ' not found');
        return done(null, false, {
          message: 'User not found'
        });
      }

      // TODO: Use crypto hash and salt
      // if (!isValidPassword(user, password)) {
      //   console.log('Invalid password');
      //   return done(null, false, {
      //     message: 'Invalid Password'
      //   });
      // }

      return done(null, user);
    });
  }));

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated())
    return next();

  res.sendStatus(401);
}

//log tinify errors
function errorLog(err) {
  if (err instanceof tinify.AccountError) {
    console.log("AccountError: " + err.message);
    // Verify your API key and account limit.
  } else if (err instanceof tinify.ClientError) {
    console.log("ClientError: " + err.message);
    // Check your source image and request options.
  } else if (err instanceof tinify.ServerError) {
    console.log("ServerError: " + err.message);
    // Temporary issue with the Tinify API.
  } else if (err instanceof tinify.ConnectionError) {
    console.log("ConnectionError: " + err.message);
    // A network connection error occurred.
  } else {
    console.log("Unknown error: " + err);
    // Something else went wrong, unrelated to the Tinify API.
  }
  proc.stdout.pipe(tinify_err_log);
}

router.post("/login",
  passport.authenticate("local-login"),
  function (req, res) {
    console.log('successful', req.body)
    res.status(200).send({
      'username': req.body.username
    });
  });

router.post('/whatisthis', isLoggedIn, (req, res) => {
  console.log('whatisthis<<');
  let input = req.body.base64.split(';base64,').pop();
  let type = req.body.base64.split(';')[0];
  let regex = /data:image\//g;
  type = type.split(regex)[1];
  let timestamp = new Date();
  const fname = 'upload_' + timestamp.getTime() + '_.' + type;
  console.log('fname', fname);
  fs.writeFile(fname, input, {
    encoding: 'base64'
  }, function (err) {
    if (err) {
      console.log('writeFile: ' + err);
    }
  });
  fs.readFile(fname, function (err, source) {
    if (err) {
      errorLog(err);
    } else {
      tinify.fromBuffer(source).toBuffer(function (err, resultImg) {
        if (err) {
          errorLog(err);
        } else {
          console.log('compressed');
          resultImg = Buffer.from(resultImg).toString('base64');
          console.log('encoded');
          clarifaiApp.models.predict(Clarifai.GENERAL_MODEL, {
            base64: resultImg
          }).then(
            function (response) {
              // do something with response
              console.log('clarifai success');
              res.status(200).send(response);
            },
            function (err) {
              // there was an error
              console.log('clarifai error', err);
              proc.stdout.pipe(clarifai_err_log);
              res.status(400).send(err);
            }
          );
        }
      });
    }
  });
});

router.post('/saverecipe', isLoggedIn, function (req, res) {
  console.log('saverecipe<<', req.body);
  db.collection('visualrecipes').save(req.body, function (err, result) {
    if (err) {
      console.log(err);
    } else {
      console.log('added to database');
      res.status(200).send(result);
    }
  });

});

module.exports = router;
