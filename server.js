// Get dependencies
const express       = require('express');
const path          = require('path');
const http          = require('http');
const bodyParser    = require('body-parser');
var passport        = require('passport');
var LocalStrategy   = require('passport-local').Strategy;
var session         = require('express-session');
var cors            = require('cors');

const app = express();

// Parsers for POST data
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));


// initialize passposrt and and session for persistent login sessions
app.use(session({
  secret: "tHiSiSasEcRetStr",
  resave: true,
  saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

app.use('*',cors());

// Get our API routes
const api = require('./server/routes/api');

// Set our api routes
app.use('/api', api);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  console.log('request received');
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});




/**
 * Get port from environment and store in Express.
 */
const port = '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));